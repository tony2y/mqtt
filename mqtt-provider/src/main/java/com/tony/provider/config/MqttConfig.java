package com.tony.provider.config;//package com.tony.common.config;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * mqtt 客户端注册
 *
 * @author tony
 * @date 2021-9-15
 */
@Configuration
public class MqttConfig {

    @Value("${spring.mqtt.username}")
    private String username;

    @Value("${spring.mqtt.password}")
    private String password;

    @Value("${spring.mqtt.url}")
    private String hostUrl;

    @Value("${spring.mqtt.client.id}")
    private String clientId;

    @Value("${spring.mqtt.completionTimeout}")
    private int completionTimeout;

    @Value("${spring.mqtt.keepalive}")
    private int keepalive;


    @Bean
    public MqttClient mqttClient() {
        MqttClient client = null;
        try {
            client = new MqttClient(hostUrl, clientId, new MemoryPersistence());
            MqttConnectOptions option = new MqttConnectOptions();
            // 设置是否清空session,这里如果设置为false表示服务器会保留客户端的连接记录，
            // 这里设置为true表示每次连接到服务器都以新的身份连接
            option.setCleanSession(false);
            option.setUserName(username);
            option.setPassword(password.toCharArray());
            option.setConnectionTimeout(completionTimeout);
            option.setKeepAliveInterval(keepalive);
            option.setAutomaticReconnect(true);
            client.setCallback(new MqttPushCallback());
            client.connect(option);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return client;
    }
}
