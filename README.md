
# 项目说明
    MQTT集成SpringBoot，mqtt相比于http，它的协议开销比较小，属于轻量级协议，适用于消息分散的场景
# 应用场景
    移动互联网(MI)、物联网(IoT)
# 快速开始
    1.下载安装mqtt服务端

        win: https://mosquitto.org/download/

    2.更改配置文件中mqtt服务地址、账号密码

