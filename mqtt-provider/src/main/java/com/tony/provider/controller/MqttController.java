package com.tony.provider.controller;

import com.tony.provider.config.MqttPushClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * MQTT消息发送
 *
 * @author tony
 * @date 2021-9-15
 */
@RestController
@RequestMapping("/mqtt")
public class MqttController {

    @Value("${mqtt.topic}")
    private String topic;

    /**
     * 发送MQTT消息
     *
     * @param message 消息内容
     * @return 返回
     */
    @GetMapping(value = "/send")
    public ResponseEntity<String> sendMqtt(@RequestParam(value = "message") String message) throws MqttException {
        MqttPushClient.getInstance().publish(topic,message);
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }
}
