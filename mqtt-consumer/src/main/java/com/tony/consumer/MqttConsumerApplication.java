package com.tony.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * mqtt 消息接收端启动入口
 *
 * @author tony
 * @date 2021-9-15
 */
@SpringBootApplication
public class MqttConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MqttConsumerApplication.class, args);
    }

}
