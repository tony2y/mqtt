package com.tony.provider.config;//package com.tony.common.config;


import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * 消息回调处理
 *
 * @author tony
 * @date 2021-9-15
 */
@Slf4j
public class MqttPushCallback implements MqttCallback {

    @Override
    public void connectionLost(Throwable cause) {
        log.error("断开连接，建议重连", cause);
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        try {
            log.info(token.getMessage().toString());
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    /**
     * 接收消息
     *
     * @param topic
     * @param message
     * @throws Exception
     */
    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        try {
        } catch (Exception e) {
            log.error("处理订阅消息失败", e);
        }
    }
}