package com.tony.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * mqtt 消息发送端启动入口
 *
 * @author tony
 * @date 2021-9-15
 */
@SpringBootApplication
public class MqttProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(MqttProviderApplication.class, args);
    }

}
